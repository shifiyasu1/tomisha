const List<Map<String, dynamic>> dummyData = [
  {
    "title": "Drei einfache Schritte\n zu deinem neuen Job",
    "body": [
      {
        "path": "assets/images/section_1.png",
        "title": "Erstellen dein Lebenslauf",
      },
      {
        "path": "assets/images/section_2.png",
        "title": "Erstellen dein Lebenslauf",
      },
      {
        "path": "assets/images/section_3.png",
        "title": "Mit nur einem Klick bewerben",
      },
    ],
  },
  {
    "title": "Drei einfache Schritte\n zu deinem neuen Mitarbeiter",
    "body": [
      {
        "path": "assets/images/section_1.png",
        "title": "Erstellen dein Unternehmensprofil",
      },
      {
        "path": "assets/images/section_5.png",
        "title": "Erstellen ein Jobinserat",
      },
      {
        "path": "assets/images/section_6.png",
        "title": "Wahle deinene neuen Mitarbeiter aus",
      },
    ],
  },
  {
    "title": "Drei einfache Schritte zur\n Vermittlung neuer Mitarbeiter",
    "body": [
      {
        "path": "assets/images/section_1.png",
        "title": "Erstellen dein Unternehmensprofil",
      },
      {
        "path": "assets/images/section_8.png",
        "title": "Erhalte Vermittlungs-angebot von Arbeigeber",
      },
      {
        "path": "assets/images/section_9.png",
        "title": "Vermittlung nach Provision oder Studenlohn",
      },
    ],
  },
];
