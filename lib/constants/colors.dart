import 'dart:ui';

const kGreyColor = Color(0xFF4A5568);
const kGreyLighterColor = Color(0xFF718096);
const kTealColor = Color(0xFF81E6D9);
const kTealMediumColor = Color(0xFFE6FFFA);
const kTealLighterColor = Color(0xFFCBD5E0);
const kTealDarkerColor = Color(0xFF319795);
const kBlueColor = Color(0xFF3182CE);
