import 'package:flutter/material.dart';
import 'package:tomisha/constants/colors.dart';
import 'package:tomisha/utils/size_utils.dart';

class CustomAppbar extends StatefulWidget implements PreferredSizeWidget {
  const CustomAppbar({
    super.key,
    required this.showScrolledContent,
  });
  final bool showScrolledContent;
  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  State<CustomAppbar> createState() => _CustomAppbarState();
}

class _CustomAppbarState extends State<CustomAppbar> {
  bool isHovering = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          height: 3,
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                kBlueColor,
                kTealDarkerColor,
              ],
            ),
          ),
        ),
        AppBar(
          elevation: 5,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
            ),
          ),
          shadowColor: Colors.black.withOpacity(.2),
          surfaceTintColor: Colors.white.withOpacity(.0),
          actions: [
            (widget.showScrolledContent)
                ? Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      "Jetzt Klicken",
                      style: TextStyle(
                        color: kGreyLighterColor,
                        fontSize: SizeUtils.getDisplayWidth(context) * .01,
                      ),
                    ),
                  )
                : Container(),
            (widget.showScrolledContent)
                ? Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.black.withOpacity(.1),
                        ),
                        borderRadius: BorderRadius.circular(10)),
                    child: Text(
                      "Kostenlos Registrieren",
                      style: TextStyle(
                        decorationColor: const Color(0xFF83CFCB),
                        fontSize: SizeUtils.getDisplayWidth(context) * .01,
                        color: const Color(0xFF83CFCB),
                      ),
                    ),
                  )
                : Container(),
            MouseRegion(
              onEnter: (event) {
                setState(() {
                  isHovering = true;
                });
              },
              onExit: (event) {
                setState(() {
                  isHovering = false;
                });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  "Login",
                  style: TextStyle(
                    decoration: isHovering
                        ? TextDecoration.underline
                        : TextDecoration.none,
                    fontSize: SizeUtils.getDisplayWidth(context) * .01,
                    decorationColor: const Color(0xFF83CFCB),
                    color: const Color(0xFF83CFCB),
                  ),
                ),
              ),
            )
          ],
          // shape: const Border(
          //   top: BorderSide(color: Colors.blue, width: 3),
          // ),
        ),
      ],
    );
  }
}
