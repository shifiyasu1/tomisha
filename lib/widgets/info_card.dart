import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tomisha/constants/colors.dart';
import 'package:tomisha/utils/size_utils.dart';

class InfoCard extends StatelessWidget {
  const InfoCard({
    super.key,
    required this.title,
    required this.imagePath,
    required this.backgroundColor,
    this.isImageFirst = false,
    required this.order,
  });
  final String title;
  final String imagePath;
  final Color backgroundColor;
  final bool isImageFirst;
  final int order;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(color: backgroundColor),
      child: SizeUtils.isMobile(context)
          ? Column(
              children: [
                Image.asset(
                  imagePath,
                  width: SizeUtils.getDisplayWidth(context) * .45,
                ),
                RichText(
                  overflow: TextOverflow.clip,
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.ltr,
                  softWrap: true,
                  maxLines: 1,
                  textScaleFactor: 1,
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: "$order.",
                        style: GoogleFonts.lato(
                          fontSize: SizeUtils.getDisplayWidth(context) * .1,
                          color: kGreyLighterColor,
                        ),
                      ),
                      TextSpan(
                        text: title,
                        style: GoogleFonts.lato(
                          fontSize: SizeUtils.getDisplayWidth(context) * .02,
                          color: kGreyLighterColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Spacer(
                  flex: 2,
                ),
                isImageFirst
                    ? Image.asset(
                        imagePath,
                        width: SizeUtils.getDisplayWidth(context) * .45,
                      )
                    : Container(),
                isImageFirst ? const Spacer() : Container(),
                RichText(
                  overflow: TextOverflow.clip,
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.ltr,
                  softWrap: true,
                  maxLines: 1,
                  textScaleFactor: 1,
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: "$order.",
                        style: GoogleFonts.lato(
                          fontSize: SizeUtils.getDisplayWidth(context) * .1,
                          color: kGreyLighterColor,
                        ),
                      ),
                      TextSpan(
                        text: title,
                        style: GoogleFonts.lato(
                          fontSize: SizeUtils.getDisplayWidth(context) * .02,
                          color: kGreyLighterColor,
                        ),
                      ),
                    ],
                  ),
                ),
                isImageFirst
                    ? Container()
                    : Image.asset(
                        imagePath,
                        width: SizeUtils.getDisplayWidth(context) * .38,
                      ),
                isImageFirst ? Container() : const Spacer(),
                const Spacer(
                  flex: 3,
                ),
              ],
            ),
    );
  }
}
