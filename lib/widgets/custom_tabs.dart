import 'package:flutter/material.dart';
import 'package:tomisha/constants/colors.dart';
import 'package:tomisha/utils/size_utils.dart';

class CustomTabs extends StatefulWidget {
  const CustomTabs({
    super.key,
    required this.onChanged,
    required this.selectedTab,
  });
  final Function(int) onChanged;
  final int selectedTab;

  @override
  State<CustomTabs> createState() => _CustomTabsState();
}

class _CustomTabsState extends State<CustomTabs> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SingleTabWidget(
          isSelected: widget.selectedTab == 0,
          title: "Arbeitnehmer",
          borderRadiusGeometry: const BorderRadius.only(
            topLeft: Radius.circular(15),
            bottomLeft: Radius.circular(15),
          ),
          onClick: () {
            widget.onChanged(0);
          },
        ),
        SingleTabWidget(
          isSelected: widget.selectedTab == 1,
          title: "Arbeitgeber",
          borderRadiusGeometry: const BorderRadius.all(Radius.zero),
          onClick: () {
            widget.onChanged(1);
          },
        ),
        SingleTabWidget(
          isSelected: widget.selectedTab == 2,
          title: "Temporärbüro",
          borderRadiusGeometry: const BorderRadius.only(
            topRight: Radius.circular(15),
            bottomRight: Radius.circular(15),
          ),
          onClick: () {
            widget.onChanged(2);
          },
        ),
      ],
    );
  }
}

class SingleTabWidget extends StatelessWidget {
  final bool isSelected;
  final String title;
  final BorderRadiusGeometry borderRadiusGeometry;
  final Function onClick;
  const SingleTabWidget({
    super.key,
    required this.isSelected,
    required this.title,
    required this.borderRadiusGeometry,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onClick();
      },
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 100),
        padding: EdgeInsets.symmetric(
            horizontal: SizeUtils.getDisplayWidth(context) * .02, vertical: 15),
        decoration: BoxDecoration(
            color: isSelected ? kTealColor : Colors.white,
            borderRadius: borderRadiusGeometry,
            border: Border.all(color: kTealLighterColor, width: .2)),
        child: Text(
          title,
          style: TextStyle(
            fontSize: SizeUtils.getDisplayWidth(context) * .012,
            color: isSelected ? Colors.white : kTealDarkerColor,
          ),
        ),
      ),
    );
  }
}
