import 'package:flutter/material.dart';
import 'package:tomisha/constants/colors.dart';
import 'package:tomisha/utils/size_utils.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    super.key,
    required this.title,
    required this.onClick,
  });
  final String title;
  final Function onClick;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onClick,
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: SizeUtils.getDisplayWidth(context) * .03, vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [kTealDarkerColor, kBlueColor],
          ),
        ),
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: SizeUtils.getDisplayWidth(context) * .01,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
