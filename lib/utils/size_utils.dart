import 'package:flutter/material.dart';

class SizeUtils {
  static Size displaySize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  static double getDisplayHeight(BuildContext context) {
    return displaySize(context).height -
        MediaQuery.of(context).padding.top -
        MediaQuery.of(context).padding.bottom;
  }

  static double getDisplayWidth(BuildContext context) {
    return displaySize(context).width -
        MediaQuery.of(context).padding.left -
        MediaQuery.of(context).padding.right;
  }

  static bool isMobile(BuildContext context) {
    return displaySize(context).width < 550;
  }
}
