import 'package:flutter/material.dart';
import 'package:tomisha/constants/colors.dart';
import 'package:tomisha/data/dummy_data.dart';
import 'package:tomisha/utils/size_utils.dart';
import 'package:tomisha/widgets/custom_appbar.dart';
import 'package:tomisha/widgets/custom_button.dart';
import 'package:tomisha/widgets/custom_tabs.dart';
import 'package:tomisha/widgets/info_card.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedTab = 0;
  bool showScrolledContent = false;
  ScrollController scrollController = ScrollController();

  void showAndHideAppbarContent() {
    if (scrollController.offset > 300) {
      if (!showScrolledContent) {
        setState(() {
          showScrolledContent = true;
        });
      }
    } else {
      if (showScrolledContent) {
        setState(() {
          showScrolledContent = false;
        });
      }
    }
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        showScrolledContent: showScrolledContent,
      ),
      body: Stack(
        children: [
          Listener(
            onPointerSignal: (event) {
              showAndHideAppbarContent();
            },
            child: ListView(
              controller: scrollController,
              children: [
                //Hero Section
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 0, vertical: 50),
                  // height: MediaQuery.of(context).size.height * .4,
                  color: Colors.teal.withOpacity(.3),
                  child: SizeUtils.isMobile(context)
                      ? Column(
                          children: [
                            Text(
                              "Deine Job\nwebsite",
                              style: TextStyle(
                                  fontSize:
                                      SizeUtils.getDisplayWidth(context) * .05,
                                  fontWeight: FontWeight.bold),
                            ),
                            Image.asset("assets/images/handshake.png"),
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            // const Spacer(),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Deine Job\nwebsite",
                                  style: TextStyle(
                                      fontSize:
                                          SizeUtils.getDisplayWidth(context) *
                                              .04,
                                      fontWeight: FontWeight.bold),
                                ),
                                CustomButton(
                                  onClick: () {},
                                  title: "Kostenlos Registrieren",
                                )
                              ],
                            ),
                            CircleAvatar(
                              maxRadius:
                                  SizeUtils.getDisplayWidth(context) * .1,
                              backgroundColor: Colors.white,
                              child: Image.asset("assets/images/handshake.png"),
                            ),
                            SizedBox(
                              width: SizeUtils.getDisplayWidth(context) * .1,
                            )
                            // const Spacer(
                            //   flex: 7,
                            // ),
                          ],
                        ),
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: CustomTabs(
                    selectedTab: selectedTab,
                    onChanged: (value) {
                      setState(() {
                        selectedTab = value;
                      });
                    },
                  ),
                ),
                Text(
                  dummyData[selectedTab]["title"],
                  style: TextStyle(
                      fontSize: SizeUtils.getDisplayWidth(context) * .04,
                      color: kGreyColor,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                ),
                ...List.generate(
                  3,
                  (index) => InfoCard(
                    imagePath: dummyData[selectedTab]["body"][index]["path"],
                    title: dummyData[selectedTab]["body"][index]["title"],
                    backgroundColor:
                        index == 1 ? kTealMediumColor : Colors.white,
                    isImageFirst: index == 1,
                    order: index + 1,
                  ),
                ),
              ],
            ),
          ),
          SizeUtils.isMobile(context)
              ? Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.red,
                            blurRadius: 4,
                            offset: Offset(4, 8), // Shadow position
                          ),
                        ],
                        border: Border.all(color: Colors.black.withOpacity(.1)),
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20))),
                    padding: EdgeInsets.only(
                      left: SizeUtils.getDisplayWidth(context) * .1,
                      right: SizeUtils.getDisplayWidth(context) * .1,
                      top: SizeUtils.getDisplayHeight(context) * .04,
                      bottom: SizeUtils.getDisplayHeight(context) * .02,
                    ),
                    child: CustomButton(
                      onClick: () {},
                      title: "Kostenlos Registrieren",
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
